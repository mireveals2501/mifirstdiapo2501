![ciobes](img/unamed2.png) 
# études à l'étranger et mobilité à l'international


Les mobilités d'un séjour scolaire à l'étranger peuvent recouvrir des formes multiples : individuelles ou collectives, brèves ou longues : échanges, voyages de classes, périodes de scolarité à l'étranger, séquences d'observation, stages ou périodes de formation en milieu professionnel à l'étranger, volontariats de solidarité, service civique, chantiers bénévoles...

- Pour les parents , comment faire ? 

- Pour les jeunes quelles infos ?




--

# Partir à l'étranger : Repères pour les parents


[partir-l-etranger-reperes-pour-les-parents](https://eduscol.education.fr/1103/partir-l-etranger-reperes-pour-les-parents)

--

# Partir à l'étranger : Repères pour les jeunes INFOS GENERALES
<!-- .slide: data-background-image="https://www.shutterstock.com/image-photo/paper-ships-260nw-649498291.jpg" -->
[EUROGUIDANCE](https://www.euroguidance-france.org/)

[AGITATEURS DE mobilité](https://www.agitateursdemobilite.fr/)

[MOBIDATA](https://cii.edu.ac-lyon.fr/spip/spip.php?rubrique14) 

[IJBOX les programmes européens](https://www.ijbox.fr/dossiers/les-programmes-europeens) 

---

# Comment ça marche ?
<!-- .slide: data-background="#ff0000" -->
1. Un projet template qui intègre reveal js dans une image docker : https://gitlab.com/formations-kgaut/template-reveal-js
2. Un sous projet, comme celui là, qui contient les slides et utilise l'image docker précédemment crée
3. La CI de ce projet publie la présentation automatiquement à chaque modification sur gitlab page.

--

# Intéret ?
- Rédaction de présentation très rapide
- Historique des modifications
- Fork pour réutiliser / faire une nouvelle version de présentation